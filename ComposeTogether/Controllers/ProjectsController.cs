﻿using ComposeTogether.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ComposeTogether.Controllers
{
    public class ProjectsController : Controller
    {
        studioHostDBEntities db = new studioHostDBEntities();
        // GET: Projects
        [Authorize]
        public ActionResult Index(string ID)
        {
            var user = User.Identity.GetUserId();
            var listofProj = db.Projects.ToList().Where(g => g.userID == user);
            
            ViewBag.data = listofProj;
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            TempData["properAccess"] = true;
            return RedirectToAction(collection["prID"], "Projects/Details");
        }
        public ActionResult SharedIndex()
        {
            var user = User.Identity.Name;
            var listofAllowRules = db.ProjectsAndAllowedUsers.Where(p => p.userID == user).ToList();
            List<Project> listOfProjects = new List<Project>();
            foreach (var item in listofAllowRules)
            {
                listOfProjects.Add(db.Projects.Find(item.projectID));
            }
            ViewBag.data = listOfProjects;
       
            return View();
        }
        [HttpPost]
        public ActionResult Details(FormCollection collection)
        {
            TempData["properAccess"] = true;    
            return RedirectToAction(collection["prID"], "Projects/Edit");
        }

        [Authorize]
        // GET: Projects/Details/5
        public ActionResult Details(int id)
        {
            if(TempData["properAccess"] == null)
            {
                return RedirectToAction("Index","Projects");
            }
            if (!(bool)TempData["properAccess"])
            {
                return RedirectToAction("Index","Projects");
            }

            var listOfContributors = (from a in new studioHostDBEntities().ProjectsAndAllowedUsers
                                      where a.projectID == id
                                      select new { a.userID }).ToList();



            ViewBag.data = listOfContributors;
            //confirm that the project's allowed table has the user that is clicking on it (or they are the owner) 
            //popualate list with all sheets


            Project project = db.Projects.Find(id);
            var listOfSheets = db.Sheets.Where(s => s.projectID == project.projectID).OrderByDescending(x => x.lastTouched);
            ViewBag.listOfSheets = listOfSheets;
            List<String> listofContribStrings = new List<string>();
            foreach (var item in listOfContributors)
            {
                listofContribStrings.Add(item.userID.ToString());
                
            }

            ViewBag.userid = User.Identity.GetUserId();

            if (!listofContribStrings.Contains(User.Identity.GetUserName()) && project.userID != User.Identity.GetUserId() && project.privateProj == true)
            {
                return View();
            }

            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }
        [Authorize]
        // GET: Projects/Create
        public ActionResult Create()
        {
            return View();
        }
        [Authorize]
        // POST: Projects/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                Project newProj = new Project();
                bool privateProj = false;
                if (collection["privateProj"] != null) {
                
                privateProj = Convert.ToBoolean(collection["privateProj"].Split(',')[0]);
                 }



                newProj.privateProj = privateProj;
                newProj.projDescrip = collection["projDescrip"];
                newProj.projTitle = collection["projTitle"];
                newProj.userID =  User.Identity.GetUserId();

                bool allowEdit = false;
                if (collection["allowEdit"] != null)
                {
                    allowEdit = Convert.ToBoolean(collection["allowEdit"].Split(',')[0]);
                }
                
                newProj.allowEdit = allowEdit;

                db.Projects.Add(newProj);


                db.SaveChanges();

                if (privateProj) //IF it is a private project
                {
                     int listelemnumber = 0;
                     while (listelemnumber < 10)
                     {


                        ProjectsAndAllowedUser projAllow = new ProjectsAndAllowedUser();
                        projAllow.projectID = newProj.projectID;
                        
                        projAllow.userID = collection["listelemtext" + listelemnumber]; //USER NAME
                        if (projAllow.userID == null)
                        {
                            listelemnumber++;
                            continue;
                        }


                        listelemnumber++;
                        db.ProjectsAndAllowedUsers.Add(projAllow);
                        db.SaveChanges();
                        


                     }

                }



                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [Authorize]
        // GET: Projects/Edit/5
        public ActionResult Edit(int id)
        {

            if (TempData["properAccess"] == null)
            {
                return RedirectToAction("Index", "");
            }
            if (!(bool)TempData["properAccess"])
            {
                return RedirectToAction("Index", "");
            }

            Project curProject = db.Projects.Find(id);
            ViewBag.curProject = curProject;


            //get list of all users connected to this proj in allowed table, pass to ViewBag
            

            return View();
        }
        [Authorize]
        // POST: Projects/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [Authorize]
        public ActionResult Delete(int id)
        {
            db.Projects.Remove(db.Projects.Find(id));
            return View();
        }
    }
}
