﻿using ComposeTogether.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ComposeTogether.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        studioHostDBEntities db = new studioHostDBEntities();

        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var userName = User.Identity.Name;
            var listOfLastThreeUpdatedSheets = db.Sheets.Where(x => db.Projects
            .Where(y => y.userID == userId)
            .Select(z => z.projectID)
            .Contains(x.projectID)
            ||
            db.ProjectsAndAllowedUsers.Where(a => a.userID == userName)
            .Select(b => b.projectID)
            .Contains(x.projectID))
            .OrderByDescending(x=>x.lastTouched).Take(3).ToList();


            

            List<Project> listOfProjects = new List<Project>();

            List<int> idsTaken = new List<int>();

            foreach (var item in listOfLastThreeUpdatedSheets)
            {
                if(idsTaken.Contains(item.projectID))
                {
                    continue;
                }
                idsTaken.Add(item.projectID);
                listOfProjects.Add(db.Projects.Find(item.projectID));
            }
            ViewBag.data = listOfProjects;
            ////            SELECT TOP 3 *
            ////FROM Sheets
            ////Where projectId IN (Select projectId from Projects where userId = currentUserId) 
            ////OR projectId IN(Select projectId from ProjectAndAllowedUser where userId = currentUserId)) 
            ////ORDER BY lastTouched DESC;


            return View();
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Landing()
        {
            return View();
        }   
        


    }
}
