﻿using ComposeTogether.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ComposeTogether.Controllers
{
    public class CreateController : Controller
    {
        studioHostDBEntities db = new studioHostDBEntities();
        // GET: Create
        [Authorize]
        public ActionResult Index(int id)
        {

            var sheet = db.Sheets.Find(id);
            var proj = db.Projects.Find(sheet.projectID);
            var username = User.Identity.Name;
            var listOfAllowedUsers = db.ProjectsAndAllowedUsers.Where(p => p.projectID == sheet.projectID && p.userID == username);
            if ((listOfAllowedUsers.Any() && proj.allowEdit) || proj.userID == User.Identity.GetUserId() || (proj.allowEdit && !proj.privateProj)) //they are allowed to edit
            {

                ViewBag.notationText = sheet.notationText;
                ViewBag.sheetTitle = sheet.title;
                ViewBag.username = User.Identity.Name.ToString().Split('@')[0];
                ViewBag.sheetID = sheet.sheetID;
                ViewBag.curBeats = sheet.currentBeats;
                String currentKey = sheet.curKey;
                ViewBag.curKey = sheet.curKey.ToString();
                Console.Write("s");
                return View();
            }
            else if(listOfAllowedUsers.Any() && !proj.allowEdit) // just view
            {
                return RedirectToAction("ViewOnlyIndex", "Create", new { id = id });
            }
            else
            {
                return RedirectToAction("Index", "Projects");
            }   

        }
        public ActionResult ViewOnlyIndex(int id)
        {
            var sheet = db.Sheets.Find(id);
            ViewBag.notationText = sheet.notationText;
            ViewBag.sheetID = sheet.sheetID;
            ViewBag.sheetTitle = sheet.title;
            ViewBag.username = User.Identity.Name.ToString().Split('@')[0];
            ViewBag.curBeats = sheet.currentBeats;
            String currentKey = sheet.curKey;
            return View();

        }
        [Authorize]
        public ActionResult CreateNew(int id)
        {
            ViewBag.projectID = id;

            return View();
        }
        [HttpPost]
        public ActionResult CreateNew(FormCollection collection)
        {
            Sheet s = new Sheet();
            s.genre = collection["genre"];
            s.title = collection["title"];

            s.projectID = int.Parse(collection["shnoo"]);
            String initialNotation = "stave time=" + collection["TimeSignature"] + "/4 key=C clef=" + collection["Clef"].ToLower() + "\r\n" + "notes =|| ";

            s.notationText = initialNotation;
            DateTime myDateTime = DateTime.UtcNow;
            s.lastTouched = myDateTime;
            s.curKey = "C";
            s.currentBeats = "0";
            db.Sheets.Add(s);
            db.SaveChanges();
            TempData["properAccess"] = true;
            return RedirectToAction("Details", "Projects", new { id = s.projectID });
        }

    }
}