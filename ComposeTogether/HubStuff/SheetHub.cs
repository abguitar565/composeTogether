﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;
using ComposeTogether.Models;

namespace ComposeTogether
{

    [HubName("sheetHub")]
    public class SheetHub : Hub
    {
        studioHostDBEntities db = new studioHostDBEntities();
        public Task JoinRoom(string sheetId)
        {
            return Groups.Add(Context.ConnectionId, sheetId);
            
        }

        public Task LeaveRoom(string sheetId)
        {
            return Groups.Remove(Context.ConnectionId, sheetId);
        }

        public void Send(string name, string message, string sheetID)
        {
            // Call the broadcastMessage method to update clients.
            Clients.Group(sheetID).broadcastMessage(name, message);
        }

        public void commandParser(string commandStr,string sheetID)
        {
            //not being used yet
            Clients.Group(sheetID).idleClockReset();
            String commandString = commandStr;
            String command = commandString.Split('~')[0];
            if(command.Equals("ADD", StringComparison.InvariantCultureIgnoreCase))
            {
                string notationText = commandString.Split('~')[1];
                var beatsToAdd = float.Parse(commandString.Split('~')[2]);
                Clients.Group(sheetID).addNotation(notationText,beatsToAdd);               

            }
            else if (command.Equals("DELETE", StringComparison.InvariantCultureIgnoreCase))
            {
                int numOfNotesToDelete = int.Parse(commandString.Split('~')[1]); //#OFNOTESTODELETE
                float currentBeats = float.Parse(commandString.Split('~')[2]);

                Clients.Group(sheetID).deleteNotes(numOfNotesToDelete,currentBeats);
            }
            else if (command.Equals("KEYCHANGE", StringComparison.InvariantCultureIgnoreCase))
            {
                string newKey = commandString.Split('~')[1]; //key to change to
                Clients.Group(sheetID).keyChange(newKey);
            }
        }
        public void savetocloud(string notationText, string sheetID, string currentBeats, string curKey)
        {
            Clients.Group(sheetID).idleClockReset();
            //Clients.Group(sheetID).cloudSyncConfirmed();
            int sheetid = int.Parse(sheetID);
            Sheet s =  db.Sheets.SingleOrDefault(x => x.sheetID == sheetid);
            s.notationText = notationText;
            s.currentBeats = currentBeats;
            s.curKey = curKey;
            db.SaveChanges();
        }


        public void connectedToSheet(string userName, string sheetID)
        {
            Clients.Group(sheetID).updateConnected(userName);
        }
        public void disconnectedFromSheet(string userName, string sheetID)
        {
            Clients.Group(sheetID).updateDisconnected(userName);
        }
    }
}