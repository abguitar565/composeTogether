﻿//utilityFunctions
var unReadChat = 0;
var idleTime = 0;
$(".saving").hide();


setTimeout(function () {
    $.connection.hub.start().done(function () {
        chat.server.joinRoom("" + sheetID);
        
        $('#sendmessage').click(function () {
            // Call the Send method on the hub. 
            chat.server.send($('#displayname').val(), $('#message').val(), sheetID);
            // Clear text box and reset focus for next comment. 
            $('#message').val('').focus();
        });
        chat.server.connectedToSheet($('#displayname').val(), sheetID);
    });
    
}, 500);

var sliceToColon = function (str, removals) {
    var parts = str.split(":");
    if (parts.length == 1) {
        return str;
    }
    else
        return parts.slice(0, -removals).join(":");
}

String.prototype.insert = function (index, string) {
    if (index > 0)
        return this.substring(0, index) + string + this.substring(index, this.length);
    else
        return string + this;
};
    
        // Declare a proxy to reference the hub. 
        var chat = $.connection.sheetHub;
        $.connection.hub.logging = true;

        var scrolled = false;
        function updateScroll() {
            if (!scrolled) {
                var element = document.getElementsByClassName("text-higher")[0];
                element.scrollTop = element.scrollHeight;
            }
        }
            
        $(".text-higher").on('scroll', function () {
            if ($(".text-higher").scrollTop() + $(".text-higher").height() > $(".text-higher").height()) {
                scrolled = false;
            }
            else {
                scrolled = true;
            }

        });
        
        // Create a function that the hub can call to broadcast messages.
        chat.client.broadcastMessage = function (name, message) {
            // Html encode display name and message. 
            var encodedName = $('<div />').text(name).html();
            var encodedMsg = $('<div />').text(message).html();
            // Add the message to the page. 
            

            
            $('.text-higher').append('<li><strong>' + encodedName
                + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');

            if ($(".chat-content").css('display') == 'none') {
                unReadChat++;
                updateChatTicker();
            }

            updateScroll();
        };

        chat.client.updateConnected = function(name){
            $('.text-higher').append('<li><strong>' + name
                + ' has connected </strong>&nbsp;&nbsp;' + '</li>');
            if ($(".chat-content").css('display') == 'none') {
                unReadChat++;
                updateChatTicker();
            }
            $('#connectedUsers').append('<div id="connectedUserContainer"><div id="connectedUserBox">' + name + '</div></div>');
        }
        chat.client.updateDisconnected = function (name) {
            $('.text-higher').append('<li><strong>' + name
                + ' has disconnected </strong>&nbsp;&nbsp;' + '</li>');
        }

        var updateChatTicker = function () {
            if (unReadChat == 0) {
                $("#chatTicker").hide();
            } else {
                $("#chatTicker").show();
                $("#chatTicker").text(unReadChat);
            }
        }
        updateChatTicker();

        chat.client.addNotation = function (str,beatsToAdd) {
            currentBeatsTaken += beatsToAdd;
            if (currentBeatsTaken > beatsPerMeasure) {
                currentBeatsTaken = beatsToAdd;
            }
            $(".editor").val($('.editor').val() + str+" ");
            $(".editor").trigger('keyup');
        };
        chat.client.deleteNotes = function (numOfDelete, currentBeats) {
            currentBeatsTaken = currentBeats;
            var currentEditorContents = $(".editor").val();
          
            $(".editor").val(sliceToColon(currentEditorContents, numOfDelete));
            $(".editor").trigger('keyup');

        };
        chat.client.keyChange = function (newKey) {
            var currentEditorContents = $(".editor").val();
            var lastIndexOfKeyequal = currentEditorContents.lastIndexOf('key=');

            var curKeyLength = currentEditorContents.substring(lastIndexOfKeyequal, lastIndexOfKeyequal + 10);
            var reg = /(key=[A-F](#|b)?)/;
            var curkeylengthfound = curKeyLength.match(reg)[0].length;
            var firstHalf = "";
            var secondHalf = "";
            if (curkeylengthfound == 6) {
                var firstHalf = currentEditorContents.substring(0, lastIndexOfKeyequal)
                var secondHalf = currentEditorContents.substring(lastIndexOfKeyequal + 7, currentEditorContents.length);
            }
            else{
                var firstHalf = currentEditorContents.substring(0, lastIndexOfKeyequal)
            var secondHalf = currentEditorContents.substring(lastIndexOfKeyequal+6, currentEditorContents.length);
            }
           
            var newContent = firstHalf + "key=" + newKey + " " + secondHalf;
            $(".editor").val(newContent);
            $(".editor").trigger('keyup');

        };


        // Get the user name and store it to prepend to messages.
        
        $('#displayname').val(curname);
        // Set initial focus to message input box.  
        $('#message').focus();
        // Start the connection.

        $.connection.hub.error(function (error) { console.log('SignalR error: ' + error) });
        

        chat.client.idleClockReset = function () {
            idleTime = 0;

        };

        chat.client.cloudSyncConfirmed = function () {
            //light up div icon of a cloud when saved, dim back down once someone interacts with commandParser
        };

        function pushToCommand(str) {
            $(".sync").hide();
            $(".sync").css('opacity', '0');
            $(".saving").css('display', 'block');
            $(".saving").css('opacity', '1');
            
            idleTime = 0;
            chat.server.commandParser(str, sheetID)
        }

        var idleInterval = setInterval(timerIncrement, 300); // .3 seconds


        function timerIncrement() {
            idleTime++;
            if (idleTime >= 10) { // 3 seconds
                if ($(".editor-error").is(':empty')) {
                    $(".sync").show();
                    $(".sync").css('opacity', '1');
                    $(".saving").css('opacity', '0');
                    $(".saving").css('display', 'none')
                    chat.server.savetocloud($('.editor').val(), sheetID, "" + currentBeatsTaken, currentKey);
               
                    idleTime = 0;
                }
                else {
                    $(".sync").show();
                    $(".sync").css('opacity', '1');
                    $(".saving").css('opacity', '0');
                    $(".saving").css('display', 'none')

                    idleTime = 0;
                }
            }
        }