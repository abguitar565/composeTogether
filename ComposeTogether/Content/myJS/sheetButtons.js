
var FinalNotation = "";
var selectedNoteLength = "";
var selectedNoteLengthSave = "";
var selectedNoteLetter = "";
var selectedNoteLetterSave = "";
var selectedNoteOctave = "";
var isTiedNote = 0;

var isRest = false;

var accidental = "none";

var currentKey = "C";

var curNoteLength = 0;

var accentlocation = "top";
var accent = false;
var staccato = false;
var tenuto = false;

var currentBeatsTaken = 0;
var beatsPerMeasure = 4; // make selectable later
var barsPerLine = 5
var barsDrawnSoFar = 0;

var clef = "treble";

var regForClefAndTime = /.*time=([0-9])\/[4].*clef=(treble|bass)/


$(document).ready(function () {

    $(function () {
        beatsPerMeasure = $(".editor").val().match(regForClefAndTime)[1];
        clef = $(".editor").val().match(regForClefAndTime)[2];

        if (clef == "bass") {
            document.getElementById("4").innerHTML="<a>2</a>";
            $("#4").prop('text', '2');
            $("#4").prop('id', '2');
            
            document.getElementById("5").innerHTML = "<a>3</a>";
            $("#5").prop('text', '3');
            $("#5").prop('id', '3');
        }

        var lastStaff = $(".editor").val().lastIndexOf("notes =||");
        var lastSection = $(".editor").val().substring(lastStaff+10, $(".editor").val().length);
        barsDrawnSoFar = lastSection.replace(/[^\|]/g, "").length;

        currentBeatsTaken = curBeats;
        currentKey = curKey;

    });

    include("../../Content/myJS/signalRsheets.js");
    //utility
    function include(filename) {
        var head = document.getElementsByTagName('head')[0];

        var script = document.createElement('script');
        script.src = filename;
        script.type = 'text/javascript';

        head.appendChild(script)
    }
    //find nth index of
    function nthIndex(str, pat, n) {
        var L = str.length, i = -1;
        while (n-- && i++ < L) {
            i = str.indexOf(pat, i);
            if (i < 0) break;
        }
        return i;
    }
    //insert at index
    String.prototype.insert = function (index, string) {
        if (index > 0)
            return this.substring(0, index) + string + this.substring(index, this.length);
        else
            return string + this;
    };

    var canvas = document.getElementsByClassName('vex-canvas')[0];// in your HTML this element appears as <canvas id="mycanvas"></canvas>
    var ctx = canvas.getContext('2d');

    $(".vex-canvas").css("padding-top", "20px");
    $(".vex-canvas").css("margin-left", "auto");
    $(".vex-canvas").css("margin-right", "auto");
    $(".vex-canvas").css("display", "block")



    $("#helpbutton").click(function () {

        swal(" ", "Click on the tab labeled 'Notation Tool' to access various composition tools \n\n Click anywhere on the sheet music to add notes \n\n Try using your mouse wheel when hovering over the sheet music area to change the selected note letter.")
    })

    $("#savebutton").click(function () {
        // create `a` element
        var rawText = $('.editor').val().replace(/\n/g, '\r\n')
        $("<a />", {
            // if supported , set name of file
            download: $.now() + ".txt",
            // set `href` to `objectURL` of `Blob` of `textarea` value

            href: URL.createObjectURL(
              new Blob([rawText], {
                  type: "text/plain"
              }))

        }).appendTo("body")[0].click();
        // console.log("TextBox Value is : "+$(".editor").val())
        // append `a` element to `body`
        // call `click` on `DOM` element `a`

        // remove appended `a` element after "Save File" dialog,
        // `window` regains `focus` 
        $(window).one("focus", function () {
            $("a").last().remove()
        })
    });
    $('.vex-canvas').attr('id', 'vexcanvas');
    $('.editor').hide();

    $("#downloadSheet").click(function () {
        var button = document.getElementById('btn-download');


        var canvas = document.getElementById("vexcanvas");
        var newCanvas = document.createElement('canvas');


        var ctx = newCanvas.getContext('2d');

        newCanvas.width = canvas.width;
        newCanvas.height = canvas.height;

        ctx.drawImage(canvas, 0, 0);



        // set the ctx to draw beneath your current content
        ctx.globalCompositeOperation = 'destination-over';

        // set the fill color to white
        ctx.fillStyle = 'white';

        // apply fill starting from point (0,0) to point (canvas.width,canvas.height)
        // these two points are the top left and the bottom right of the canvas
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        var img = newCanvas.toDataURL("image/png");

        button.href = img;
        button.click();
    });


    $("#showEditorButton").click(function () {
        if ($('.editor').css('display') == 'none') {
            $('.editor').show(350);
        }
        else {
            $('.editor').hide(250);
        }
    })


    $(document).on('click', '.dropdown-menu li a', function () {

        $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
        $(this).parents(".dropdown").find('.btn').val($(this).data('value'));

    });

    //ADDING NOTES BELOW




    //SELECTORS

    $('#changeLastKey').click(function () {
        pushToCommand("KEYCHANGE~" + currentKey);
    });

    $("#sharp").click(function () {
        
        if (accidental == "sharp") {
            document.getElementById("sharp").className = document.getElementById("sharp").className.replace(" active", "");
            accidental = "none";
        } else {
            document.getElementById("sharp").className += " active";
            accidental = "sharp";
        }

        document.getElementById("flat").className = document.getElementById("flat").className.replace(" active", "");
        document.getElementById("natural").className = document.getElementById("natural").className.replace(" active", "");
       
    });
    $("#flat").click(function () {

        if (accidental == "flat") {
            document.getElementById("flat").className = document.getElementById("flat").className.replace(" active", "");
            accidental = "none"
        } else {
            document.getElementById("flat").className += " active";
            accidental = "flat"
        }
        document.getElementById("natural").className = document.getElementById("natural").className.replace(" active", "");
        document.getElementById("sharp").className = document.getElementById("sharp").className.replace(" active", "");
    
    });
    $("#natural").click(function () {

        if (accidental == "natural") {
            accidental = "none"
            document.getElementById("natural").className = document.getElementById("natural").className.replace(" active", "");
        } else {
            document.getElementById("natural").className += " active";

            accidental = "natural"
        }
        document.getElementById("flat").className = document.getElementById("flat").className.replace(" active", "");
        document.getElementById("sharp").className = document.getElementById("sharp").className.replace(" active", "");
        

    });
    //ACCENTS

    $("#staccato").click(function () {
        if (staccato) {
            staccato = false;
            document.getElementById("staccato").className = document.getElementById("staccato").className.replace(" active", "");
        } else {
            document.getElementById("staccato").className += " active";
            staccato = true;
        }

    });
    $("#tenuto").click(function () {

        if (tenuto) {
            tenuto = false;
            document.getElementById("tenuto").className = document.getElementById("tenuto").className.replace(" active", "");
        } else {
            document.getElementById("tenuto").className += " active";
            tenuto = true;
        }
    });
    $("#accent").click(function () {

        if (accent) {
            accent = false;
            document.getElementById("accent").className = document.getElementById("accent").className.replace(" active", "");
        } else {
            document.getElementById("accent").className += " active";
            accent = true;
        }


    });
    $("#accentdownarrow").click(function () {
        if (accentlocation == "top") {
            accentlocation = "bottom";
            document.getElementById("accentdownarrow").className += " active";
        } else if (accentlocation == "bottom") {
            accentlocation = "top";
            
            document.getElementById("accentdownarrow").className = document.getElementById("accentdownarrow").className.replace(" active", "");
        }

    });
    //end accent lgoic


    $("#noteLengths li").on('click', function () {
        //set val
        selectedNoteLength = this.id;
        selectedNoteLengthSave = this.id;
    })
    $("#keyPicker li").on('click', function () {
        currentKey = this.id;
    })


    $("#noteLetters li").on('click', function () {
        selectedNoteLetter = this.id;
        selectedNoteLetterSave = this.id;
    })
    $("#noteOctaves li").on('click', function () {
        selectedNoteOctave = this.id;
    })

    //Delete Last Note

    var sliceToColon = function (str, removals) {
        var parts = str.split(":");
        if (parts.length == 1) {
            return str;
        }
        else
            return parts.slice(0, -removals).join(":");
    }

    $("#deleteLastNote").on('click', function () {

        var str = "";
        str = $(".editor").val();
        if (str.includes(":")) {
            str = str.substring(str.lastIndexOf(":"));
            var reg = /(\d+)(d?)/;
            var foundval = str.match(reg)[1];
            var isDotted = str.match(reg)[2];
            var numValue = 0;
            numValue = (beatsPerMeasure / foundval)


            if (isDotted == "d") {
                numValue = numValue + (numValue / 2);
            }
            
            if (currentBeatsTaken == 0) { 
                currentBeatsTaken = beatsPerMeasure - numValue
            }
            else {
                currentBeatsTaken = currentBeatsTaken - numValue;
            }

            pushToCommand("DELETE~1~" + currentBeatsTaken);
        }
        else
            return swal({
                title: " ",
                text: "Nothing to delete!",
                type: "error",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oops!",
                closeOnConfirm: false,
            });

    })




    //Mouse Wheel On Canvas
    $(".vex-canvas").on('wheel', function (e) {

        var delta = e.originalEvent.deltaY;
        if (delta > 0) {
            if ($("#noteLetterSelectButton").text().includes("Select Note")) {
                $("#noteLetterSelectButton").html($(this).text() + 'G <span class="caret"></span>');
                selectedNoteLetter = "G";
            }
            else if ($("#noteLetterSelectButton").text().includes("A")) {
                $("#noteLetterSelectButton").html($(this).text() + 'G <span class="caret"></span>');
                selectedNoteLetter = "G";
            }
            else if ($("#noteLetterSelectButton").text().includes("B")) {
                $("#noteLetterSelectButton").html($(this).text() + 'A <span class="caret"></span>');
                selectedNoteLetter = "A";
            }
            else if ($("#noteLetterSelectButton").text().includes("C")) {
                $("#noteLetterSelectButton").html($(this).text() + 'B <span class="caret"></span>');
                selectedNoteLetter = "B";
            }
            else if ($("#noteLetterSelectButton").text().includes("D")) {
                $("#noteLetterSelectButton").html($(this).text() + 'C <span class="caret"></span>');
                selectedNoteLetter = "C";
            }
            else if ($("#noteLetterSelectButton").text().includes("E")) {
                $("#noteLetterSelectButton").html($(this).text() + 'D <span class="caret"></span>');
                selectedNoteLetter = "D";
            }
            else if ($("#noteLetterSelectButton").text().includes("F")) {
                $("#noteLetterSelectButton").html($(this).text() + 'E <span class="caret"></span>');
                selectedNoteLetter = "E";
            }
            else if ($("#noteLetterSelectButton").text().includes("G")) {
                $("#noteLetterSelectButton").html($(this).text() + 'F <span class="caret"></span>');
                selectedNoteLetter = "F";
            }


        }
        else {
            if ($("#noteLetterSelectButton").text().includes("Select Note")) {
                $("#noteLetterSelectButton").html($(this).text() + 'A <span class="caret"></span>');
                selectedNoteLetter = "A";
            }
            else if ($("#noteLetterSelectButton").text().includes("A")) {
                $("#noteLetterSelectButton").html($(this).text() + 'B <span class="caret"></span>');
                selectedNoteLetter = "B";
            }
            else if ($("#noteLetterSelectButton").text().includes("B")) {
                $("#noteLetterSelectButton").html($(this).text() + 'C <span class="caret"></span>');
                selectedNoteLetter = "C";
            }
            else if ($("#noteLetterSelectButton").text().includes("C")) {
                $("#noteLetterSelectButton").html($(this).text() + 'D <span class="caret"></span>');
                selectedNoteLetter = "D";
            }
            else if ($("#noteLetterSelectButton").text().includes("D")) {
                $("#noteLetterSelectButton").html($(this).text() + 'E <span class="caret"></span>');
                selectedNoteLetter = "E";
            }
            else if ($("#noteLetterSelectButton").text().includes("E")) {
                $("#noteLetterSelectButton").html($(this).text() + 'F <span class="caret"></span>');
                selectedNoteLetter = "F";
            }
            else if ($("#noteLetterSelectButton").text().includes("F")) {
                $("#noteLetterSelectButton").html($(this).text() + 'G <span class="caret"></span>');
                selectedNoteLetter = "G";
            }
            else if ($("#noteLetterSelectButton").text().includes("G")) {
                $("#noteLetterSelectButton").html($(this).text() + 'A <span class="caret"></span>');
                selectedNoteLetter = "A";
            }
        }
        $("#noteLetterSelectButton").css('background-color', 'rgb(239, 232, 78)');
        setTimeout(function () {
            $("#noteLetterSelectButton").css('background-color', '#393a35');
        }, 300);
        selectedNoteLetterSave = selectedNoteLetter;
        return false; // this line is only added so the whole page won't scroll in the demo
    })

    $(".vex-canvas").on('click', function () {

        var lyric = "";

        var stringToPush = "";
        var beatsToAdd = 0;

        if (selectedNoteLength == ":1") {
            beatsToAdd += 4
        } else if (selectedNoteLength == ":2") {
            beatsToAdd += 2
        } else if (selectedNoteLength == ":4") {

            beatsToAdd += 1
        }
        else if (selectedNoteLength == ":8") {
            beatsToAdd += .5
        }
        else if (selectedNoteLength == ":16") {
            beatsToAdd += .25
        }
        else if (selectedNoteLength == ":32") {
            beatsToAdd += .125
        }

        if ($("#dottednote").hasClass('active')) {
            selectedNoteLength += "d"
            beatsToAdd = beatsToAdd + (beatsToAdd / 2);
        }   
        
        if (currentBeatsTaken + beatsToAdd > beatsPerMeasure) {

            if(currentBeatsTaken!=beatsPerMeasure){
                return swal({
                    title: "Too many beats!",
                    text: (beatsPerMeasure-currentBeatsTaken)+" Beat(s) remaining. You wanted to add "+beatsToAdd,
                    type: "error",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok!",
                    closeOnConfirm: false,
                });
                return false;
            }

            stringToPush += "|";
            barsDrawnSoFar++;
            currentBeatsTaken = 0;
        }
        if (barsDrawnSoFar == barsPerLine) {
            stringToPush += "\nstave time="+beatsPerMeasure+"/4 key=" + currentKey + " clef="+clef+"\nnotes =||";
            barsDrawnSoFar = 0;
        }
        if (selectedNoteLength == "" || selectedNoteLetter == "" || selectedNoteOctave == "") {
            return swal({
                title: " ",
                text: "Make sure you've selected a Note Length, Letter and Octave!",
                type: "error",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok!",
                closeOnConfirm: false,
            });
        }
        
            
       
        if (accidental == "flat") {
            selectedNoteLetter += "@";
        }
        if (accidental == "sharp") {
            selectedNoteLetter += "#";
        }
        if (accidental == "natural") {
            selectedNoteLetter += "n";
        }
        //if Dotted
        
       


        stringToPush += selectedNoteLength + " " + selectedNoteLetter + "/" + selectedNoteOctave + " ";

        if (tenuto) {
            stringToPush += "$.a-/"+accentlocation+".$ ";
        }
        if (accent) {
            stringToPush += "$.a>/" + accentlocation + ".$ ";
        }
        if (staccato) {
            stringToPush += "$.a./" + accentlocation + ".$ ";
        }

        if ($(".editor-error").is(':empty')) {


            //Lyric Note
            if ($("#lyricalnote").hasClass('active')) {
                swal({
                    title: "A Lyric!",
                    text: "What should be sung? ",
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: true,
                    animation: "slide-from-top",
                    inputPlaceholder: "Fi-ga-ro, each must be a seperate note"
                }, function (inputValue) {
                    lyric = inputValue;

                    if (inputValue === false) return false;
                    if (inputValue === "") {
                        swal.showInputError("You need to write something!");
                        return false
                    }
                    return pushToCommand("ADD~" + stringToPush + '$' + lyric + '$' + "~" + beatsToAdd);

                });
            } else if (isRest == true) {
                //need to add rest button
                stringToPush = selectedNoteLength + " ##";
                pushToCommand("ADD~" + stringToPush + "~" + beatsToAdd);
            }
            else {
                pushToCommand("ADD~" + stringToPush + "~" + beatsToAdd);

            }
        }

       
        selectedNoteLetter = selectedNoteLetterSave;    
        selectedNoteLength = selectedNoteLengthSave;
    });


});


//for the cloud/sync/save

