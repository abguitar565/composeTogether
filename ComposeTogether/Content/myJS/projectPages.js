$(document).ready(function () {

    //Binding Title to title input field    
    $("#projTitle").on('keyup', function () {
        if ($("#projTitle").val().length <= 0) {
            $("#titleDisplay").html("-Untitled Project-");
        }
        else {
            $("#titleDisplay").html($("#projTitle").val());
        }
    })

    $("#title").on('keyup', function () {
        if ($("#title").val().length <= 0) {
            $("#titleDisplay").html("-Untitled Score-");
        }
        else {
            $("#titleDisplay").html($("#title").val());
        }
    })
    var listnumber = 0;
    //Adding allowed Users
    $('#addUser').on('click', function (e) {
            var val = $('#in').val();
            //iterate and make sure no duplicates
            
            if (val != "") {
                $('ul.list').append('<li id="listelem" ><input type="hidden" value="' + val + '" name="listelemtext' + listnumber + '"/>' + val + '<span style="color:red;vertical-align:bottom;"  id="removeListElement"><i style="vertical-align:middle;"class="glyphicon glyphicon-remove"></i></span></li>');
                $('#in').val('');
                listnumber++;
            }
          
            $(".results").on("click", "#removeListElement", function () {
                $(this).parent('li').remove();
            }); 
    })
    //if you click on an element in list, delete it
    $('#listelem').on('click','#listelem', function () {
        $(this).parent('li').remove();
    })

    //Show Add users (if private)
    $("#privateProj").click(function(){
        if ($('#privateProj').is(":checked"))
            $("#addAllowedUsersSection").show();
        else
            $("#addAllowedUsersSection").hide();
    })



    //Hack to fix the hiding/showing
    $('#privateProj').trigger('click');
    $('#privateProj').trigger('click');

});