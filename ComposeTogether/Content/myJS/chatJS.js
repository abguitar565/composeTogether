﻿var clicked = false;
$(document).on('click', '#minimize', function (e) {
    $('.chat-content', $(this).parents('.chat')).slideToggle();
    unReadChat = 0;
    updateChatTicker();
    if (clicked) {
        $("#chatcaret").css('transform', 'rotate(180deg)');
        clicked = false;
    }
    else {
        $("#chatcaret").css('transform', 'rotate(0deg)');
        clicked = true;
    }
    
});
