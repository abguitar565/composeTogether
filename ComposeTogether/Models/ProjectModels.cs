﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ComposeTogether.Models
{
    public class ProjectModels
    {
        [Key]
        public int projectID { get; set; }
        public string userID { get; set; }
        public string ProjTitle { get; set; }
        public string ProjDescrip { get; set; }
        public bool privateProj { get; set; }
        public bool isEditable { get; set; }

    }
}